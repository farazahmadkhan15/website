---
title: Open Governance
description: Open Governance is a suite of modern technologies to reform and digitize local governance. We're starting with revolutionizing how municipalities collect fees from residents for local services.

---
